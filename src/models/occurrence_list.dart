import 'occurence.dart';

class OccurrenceList {
  final List<Occurrence> _occurrences;

  OccurrenceList(this._occurrences);

  List<Occurrence> get occurrences => _occurrences;

  void addOccurence(Occurrence occurrence) {
    this._occurrences.add(occurrence);
  }

  bool removeOccurrence(int receivedId) {
    if (searchOccurrenceById(receivedId)) {
      this
          ._occurrences
          .removeWhere((occurrence) => occurrence.id == receivedId);
      return searchOccurrenceById(receivedId) ? false : true;
    } else {
      return false;
    }
  }

  bool searchOccurrenceById(int receivedId) {
    bool ok = false;
    for (var occurrence in this._occurrences) {
      if (occurrence.id == receivedId) {
        ok = true;
      }
    }
    return ok;
  }

  factory OccurrenceList.fromJson(List<dynamic> parsedJson) {
    List<Occurrence> occurrences = new List<Occurrence>();
    occurrences = parsedJson.map((i) => Occurrence.fromJson(i)).toList();
    return new OccurrenceList(occurrences);
  }

  Set<List<Occurrence>> toJson() => {_occurrences};
}
