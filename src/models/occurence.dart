import 'dart:io';

class Occurrence {
  final int _id;
  final String _dateOccurrence;
  final String _subject;
  final String _answer;
  final int _employeeId;
  final int _occurrenceTypeId;
  final int _occurrenceStatusId;
  final int _residentId;

  Occurrence(
      this._id,
      this._dateOccurrence,
      this._subject,
      this._answer,
      this._employeeId,
      this._occurrenceTypeId,
      this._occurrenceStatusId,
      this._residentId);

  Occurrence.fromJson(Map<String, dynamic> json)
      : _id = json['id'],
        _dateOccurrence = json['dateOccurrence'],
        _subject = json['subject'],
        _answer = json['answer'],
        _employeeId = json['employeeId'],
        _occurrenceTypeId = json['occurrenceTypeId'],
        _occurrenceStatusId = json['occurrenceStatusId'],
        _residentId = json['residentId'];

  Map<String, dynamic> toJson() => {
        'id': _id,
        'dateOccurrence': _dateOccurrence,
        'subject': _subject,
        'answer': _answer,
        'employeeId': _employeeId,
        'occurrenceTypeId': _occurrenceTypeId,
        'occurrenceStatusId': _occurrenceStatusId,
        'residentId': _residentId,
      };

  int get id => _id;
  String get dateOccurrence => _dateOccurrence;
  String get subject => _subject;
  String get answer => _answer;
  int get employeeId => _employeeId;
  int get occurrenceTypeId => _occurrenceTypeId;
  int get occurrenceStatusId => _occurrenceStatusId;
  int get residentId => _residentId;

  void printData() {
    stdout.write("\n\nID: $_id\n");
    stdout.write("Date_Occurrence: $_dateOccurrence\n");
    stdout.write("Subject: $_subject\n");
    stdout.write("Answer: $_answer\n");
    stdout.write("Employee_ID: $_employeeId\n");
    stdout.write("Occurrence_Type_ID: $_occurrenceTypeId\n");
    stdout.write("Occurrence_Status_ID: $_occurrenceStatusId\n");
    stdout.write("Resident_ID: $_residentId\n\n");
  }
}

// set id(int id) => _id = id;
// set dateOccurrence(DateTime dateOccurrence) =>
//     _dateOccurrence = dateOccurrence;
// set subject(String subject) => _subject = subject;
// set answer(String answer) => _answer = answer;
// set employeeId(int employeeId) => _employeeId = employeeId;
// set occurrenceTypeId(int occurrenceTypeId) =>
//     _occurrenceTypeId = occurrenceTypeId;
// set occurrenceStatusId(int occurrenceStatusId) =>
//     _occurrenceStatusId = occurrenceStatusId;
// set residentId(int residentId) => _residentId = residentId;
