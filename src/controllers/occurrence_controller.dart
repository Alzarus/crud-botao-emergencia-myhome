import 'dart:convert';
import 'dart:io';

import '../models/occurence.dart';
import '../models/occurrence_list.dart';
import '../services/occurrence_services.dart';

class OccurrenceController {
  static void showAllOccurrences() async {
    OccurrenceList occs = await getOccurrencesList();
    occs.occurrences.forEach((occurrence) => occurrence.printData());
  }

  static void addOccurrenceToJson() async {
    OccurrenceList occs = await getOccurrencesList();
    Occurrence occurrence = newOccurrence(null);
    occs.addOccurence(occurrence);
    OccurrenceServices.writeOnJson(occs);
  }

  static void removeOccurrenceFromJson() async {
    int id;
    OccurrenceList occs = await getOccurrencesList();
    stdout.write("\n\nInsert occurrence ID that you want to remove:\n");
    id = int.parse(stdin.readLineSync());
    occs.removeOccurrence(id)
        ? stdout.write("\nRemoved with success.\n")
        : stdout.write("\nNot removed or this ID was not found.\n");
    OccurrenceServices.writeOnJson(occs);
  }

  static void editOccurrencefromJson() async {
    int id;
    OccurrenceList occs = await getOccurrencesList();
    stdout.write("\n\nInsert occurrence ID that you want to edit:\n");
    id = int.parse(stdin.readLineSync());
    if (occs.searchOccurrenceById(id)) {
      Occurrence editedOccurrence = newOccurrence({"id": id});
      occs.removeOccurrence(id)
          ? occs.addOccurence(editedOccurrence)
          : doNothing();
      OccurrenceServices.writeOnJson(occs);
    } else {
      stdout.write("\nThe inserted ID was not found.\n");
    }
  }

  static Occurrence newOccurrence(Map<String, dynamic> args) {
    String dateOccurrence, subject, answer;
    int id, employeeId, occurrenceTypeId, occurrenceStatusId, residentId;

    stdout.write("\n\nInsert the data for the occurrence:\n");

    if (args == null) {
      stdout.write("\nInsert the occurrence id:\n");
      id = int.parse(stdin.readLineSync());
    } else {
      id = args["id"];
    }

    stdout.write("\nInsert the date of the occurrence:\n");
    dateOccurrence = stdin.readLineSync();

    stdout.write("\nInsert the occurrence subject:\n");
    subject = stdin.readLineSync();

    stdout.write("\nInsert the occurrence answer:\n");
    answer = stdin.readLineSync();

    stdout.write("\nInsert the employee id:\n");
    employeeId = int.parse(stdin.readLineSync());

    stdout.write("\nInsert the occurrence type id:\n");
    occurrenceTypeId = int.parse(stdin.readLineSync());

    stdout.write("\nInsert the occurrence status id:\n");
    occurrenceStatusId = int.parse(stdin.readLineSync());

    stdout.write("\nInsert the resident id:\n");
    residentId = int.parse(stdin.readLineSync());

    return new Occurrence(id, dateOccurrence, subject, answer, employeeId,
        occurrenceTypeId, occurrenceStatusId, residentId);
  }

  static Future<OccurrenceList> getOccurrencesList() async {
    Future<OccurrenceList> occurrenceList =
        OccurrenceServices.readOccurrencesFromJson();
    OccurrenceList occs;
    await occurrenceList
        .then((result) => occs = new OccurrenceList(result.occurrences));
    return occs;
  }

  static void doNothing() {
    for (int i = 0; i < 1; i++) {
      continue;
    }
  }
}
