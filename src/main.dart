import 'dart:io';

import 'controllers/occurrence_controller.dart';

Future<void> main() async {
  int exit = 0;

  while (exit != 1) {
    printMenu();
    String op = stdin.readLineSync();

    switch (op) {
      case '1':
        await OccurrenceController.showAllOccurrences();
        break;

      case '2':
        await OccurrenceController.addOccurrenceToJson();
        break;

      case '3':
        await OccurrenceController.editOccurrencefromJson();
        break;

      case '4':
        await OccurrenceController.removeOccurrenceFromJson();
        break;

      case '5':
        exit = 1;
        break;

      default:
        stdout.write("\nInvalid option!\n");
        break;
    }
  }
  stdout.write("\n\nProgram finished.\n");
}

void printMenu() {
  stdout.write("\n\nChoose your option:\n");
  stdout.write("\n1 - Show all occurrences\n");
  stdout.write("\n2 - Add an occurence\n");
  stdout.write("\n3 - Edit an occurence\n");
  stdout.write("\n4 - Remove an occurence\n");
  stdout.write("\n5 - Exit the program\n");
}
