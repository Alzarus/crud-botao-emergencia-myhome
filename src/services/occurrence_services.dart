import 'dart:async';

import 'dart:convert';
import 'dart:io';

import '../models/occurrence_list.dart';

class OccurrenceServices {
  static Future<OccurrenceList> readOccurrencesFromJson() async {
    String jsonString = await loadJsonAsset();
    final jsonResponse = json.decode(jsonString);
    OccurrenceList occurrenceList = new OccurrenceList.fromJson(jsonResponse);
    return occurrenceList;
  }

  static Future<String> loadJsonAsset() async {
    return await new File('../assets/occurrences_list.json').readAsString();
    //USE THIS LINE WHEN DEBUG ON VISUAL STUDIO
    // return await new File('assets/occurrences_list.json').readAsString();
  }

  static void writeOnJson(OccurrenceList occs) {
    var str = json.encode(occs.occurrences);
    File file = new File("../assets/occurrences_list.json");
    //USE THIS LINE WHEN DEBUG ON VISUAL STUDIO
    // File file = new File("assets/occurrences_list.json");
    file.writeAsStringSync(str);
  }
}
